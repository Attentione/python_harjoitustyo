import argparse
import gitlab
import requests


'''
    Script to delete project from Gitlab

    Example run syntax:

    python delete_repo.py --token <access token> --project <group_name/sub_group_name/project_name OR project_id> --repo delete

    
'''

def Get_Arguments():

    '''
    Function:

        Parses user given arguments from command line and writes them to  dictionary = argument_dict

        All the arguments are required except --url which has default value "https://gitlab.labranet.jamk.fi/" but it can be overwriten from commandline       
        
    returns argument_dict
    
    
    '''
    
    # Create a new ArgumentParser object
    parser = argparse.ArgumentParser()
    
    # add_argument() method defines how a single commandline argument should be parsed
    parser.add_argument('--token', help="Enter a Gitlab access token", type=str, required=True)
    parser.add_argument('--url', help="Gitlab URL.", default="https://gitlab.labranet.jamk.fi/", type=str)
    parser.add_argument('--project', help="Give projects path in format group/subgroup/project or give projects ID", type=str, required=True)
    parser.add_argument('--repo', help="Enter 'delete' to REMOVE entire gitlab repository, requires project ID or path", type=str, required=True)
   
    args = parser.parse_args()
    #Build dictionary from arguments given by user
    argument_dict = vars(args)
    
    return argument_dict



def connect_to_gitlab_and_delete_repo(argument_dict):
    #Creates connection to Gitlab server and project and deletes selected repository
    if argument_dict["repo"] == "delete":
        try:
            gitlab.Gitlab(argument_dict["url"], private_token=argument_dict["token"],api_version="4").projects.delete(argument_dict["project"])
            print("Repositio removed successfully")
        
        except requests.exceptions.ConnectionError as e:
            print(e ," , check --url argument")
            raise SystemExit

        except gitlab.exceptions.GitlabAuthenticationError as e:
            print(e,",  check --token argument")
            raise SystemExit
        
        except gitlab.exceptions.GitlabGetError as e:
            print(e," ,if error 404: check --project argument, if error 403: check --token")
            raise SystemExit
        
def main():
    argument_dict = Get_Arguments()
    project = connect_to_gitlab_and_delete_repo(argument_dict)
    
if __name__ == '__main__':
    main()
    