# HELP

## Requirements
- Python 3.4+
- python-gitlab library:
    pip install --upgrade python-gitlab
- Personal gitlab access token which looks something like "D7vkz97yyCpa3ndq3BXS"
- For Windows use python instead of python3

## Examples
#### In test replace accesstoken with your own access token

- Adds file README.md to dokumentit/10-Projektinhallinta/ folder in project testi1/testi_subgrp/core3 and reads the content from localfile toimeksiannot/README.md

python3 modifier.py --token accesstoken --project testi1/testi_subgrp/core3 --action add --file toimeksiannot/README.md --name dokumentit/10-Projektinhallinta/README.md

- Updates file README.md in folder "dokumentit/00-Tilannekatsaus/" in project "testi1/testi_subgrp/core3" with content from local file "toimeksiannot/README2.md"

python3 modifier.py --token accesstoken --project testi1/testi_subgrp/core3 --action update --file toimeksiannot/README2.md --name dokumentit/00-Tilannekatsaus/README.md

- Adds README.md file to project "testi1/a" root folder and branch "dev" with content from localfile "toimeksiannot/README.md"
- Notice that the order of the arguments doesn't matter. If you don't give --branch it automatically pushes to "master" branch

python3 modifier.py --action add --file toimeksiannot/README.md --name README.md --token accesstoken --project testi1/a  --branch dev

- Updates file README.md in root of the project 11068 with content from localfile toimeksiannot/README2.md in gitlabserver  gitlab.labranet.jamk.fi
- Notice https://gitlab.labranet.jamk.fi/ is the default value for --url argument so it's not required, but if you want to use for example gitlab.com then --url is required

python3 modifier.py --url https://gitlab.labranet.jamk.fi/ --token accesstoken --project 11068 --action update --file toimeksiannot/README2.md --name README.md

- Adds issue with title bugeja kaikkialla and with content from localfile toimeksiannot/bug-report.md to project 11071

python3 modifier.py --token accesstoken --project 11071 --action issue --file toimeksiannot/bug-report.md --name "bugeja kaikkialla"

- Deletes entire gitlab project testi1/delete_test

python3 delete_repo.py --token accesstoken --project testi1/delete_test --repo delete

- Deletes file README.md from project 11071 

python3 modifier.py --token accesstoken --project 11071 --action delete --file toimeksiannot/README2.md --name README.md

