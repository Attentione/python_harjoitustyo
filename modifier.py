import argparse
import gitlab
import requests


'''
    Script to add/delete/update files to/from Gitlab repository. Or add Issues to Gitlab repository


    Example run syntax:

    python modifier.py --token <access token> --url <Gitlab url>--project <group/project OR project id> --action <selected action> --file <path_to_file/file.md> --name <filename.md or issue tittle>

    Examples: For windows use python instead of python3. Replace accesstoken with your own Gitlab access token.

        #Adds file README.md to dokumentit/10-Projektinhallinta/ folder in project testi1/testi_subgrp/core3 and reads the content from localfile toimeksiannot/README.md

    python3 modifier.py --token accesstoken --project testi1/testi_subgrp/core3 --action add --file toimeksiannot/README.md --name dokumentit/10-Projektinhallinta/README.md

        #Updates file README.md in folder "dokumentit/00-Tilannekatsaus/" in project "testi1/testi_subgrp/core3" with content from local file "toimeksiannot/README2.md"

    python3 modifier.py --token accesstoken --project testi1/testi_subgrp/core3 --action update --file toimeksiannot/README2.md --name dokumentit/00-Tilannekatsaus/README.md

        #Adds README.md file to project "testi1/a" root folder and branch "dev" with content from localfile "toimeksiannot/README.md"
        #Notice that the order of the arguments doesn't matter. If you don't give --branch it automatically pushes to "master" branch

    python3 modifier.py --action add --file toimeksiannot/README.md --name README.md --token accesstoken --project testi1/a  --branch dev

        #Updates file README.md in root of the project 11068 with content from localfile toimeksiannot/README2.md in gitlabserver  gitlab.labranet.jamk.fi
        #Notice https://gitlab.labranet.jamk.fi/ is the default value for --url argument so it's not required, but if you want to use for example gitlab.com then --url is required

    python3 modifier.py --url https://gitlab.labranet.jamk.fi/ --token accesstoken --project 11068 --action update --file toimeksiannot/README2.md --name README.md

        #Adds issue with title bugeja kaikkialla with content from localfile toimeksiannot/bug-report.md to project 11071

    python3 modifier.py --token accesstoken --project 11071 --action issue --file toimeksiannot/bug-report.md --name "bugeja kaikkialla"

        #Deletes entire gitlab project testi1/delete_test

    python3 delete_repo.py --token accesstoken --project testi1/delete_test --repo delete

        #Deletes file README.md from project 11071

    python3 modifier.py --token accesstoken --project 11071 --action delete --file toimeksiannot/README2.md --name README.md


'''



def Get_Arguments():

    '''
    Function:

        Parses user given arguments from command line and writes them to dictionary = argument_dict

        All the arguments are required except --url which has default value "https://gitlab.labranet.jamk.fi/" and --branch which has default value "master" but they can be overwriten from commandline

        For argument --name if used with --action add/update/delete <filename.md> commits the file to root of the project and <folder/folder/filename.md> commits file to master/folder/folder/filename.md
        
        For --action argument delete --file argument isn't used, but it's still required
        
    returns argument_dict
    
    
    '''
    
    # Create a new ArgumentParser object
    parser = argparse.ArgumentParser()
    
    # add_argument() method defines how a single commandline argument should be parsed
    parser.add_argument('--token', help="Enter a Gitlab access token", type=str, required=True)
    parser.add_argument('--url', help="Gitlab URL.", default="https://gitlab.labranet.jamk.fi/", type=str)
    parser.add_argument('--project', help="Give projects path in format group/subgroup/project or give projects ID", type=str, required=True)
    parser.add_argument('--action', help="Action: add = add new file, update = replace file,  delete = delete file, issue = add new issue", type=str, required=True)
    parser.add_argument('--file', help="Enter path to a file, that you are adding format: path_to_file/file.md", type=str, required=True)
    parser.add_argument('--name', help="Enter name for issue or file you are adding or deleting. Format: for issue 'text' and for files 'filename.md' ", type=str, required=True)
    parser.add_argument('--branch', help="Enter the Gitlab branch, usable with actions add/update/delete",default="master", type=str)
   
    args = parser.parse_args()
    #Build dictionary from arguments given by user
    argument_dict = vars(args)
    
    #Check --action argument for typo or wrong syntax
    action_test = ["add","update","delete","issue"]
    if argument_dict["action"] not in action_test:
        print("Check the --action argument, use add/update/delete/issue")
        raise SystemExit
    else:
        return argument_dict


def connect_to_gitlab(argument_dict):
    #Create connection to Gitlab server and project and store it to variable
    try:
        project = gitlab.Gitlab(argument_dict["url"], private_token=argument_dict["token"],api_version="4").projects.get(argument_dict["project"])
    
    except requests.exceptions.ConnectionError as e:
        print(e ," , check --url argument")
        raise SystemExit

    except gitlab.exceptions.GitlabAuthenticationError as e:
        print(e,",  check --token argument")
        raise SystemExit
    
    except gitlab.exceptions.GitlabGetError as e:
        print(e," ,if error 404: check --project argument, if error 403: check --token")
        raise SystemExit
    #Returns variable with connection info
    return project


def create_issue(argument_dict,project):
    #This function is only used when commandline argument --action is issue
    #Creates new issue from file defined by commandline argument --file and gives the issue tittle from commandline argument --name

    if argument_dict["action"] == "issue":
        try:
            with open(argument_dict["file"],"r") as my_issue:
                description = my_issue.read()
        
            project.issues.create({'title': argument_dict["name"], 'description': description})
            print("issue added successfully")
        
        except FileNotFoundError:
            print("File cannot be found")


def delete_file(argument_dict,project):
    #This function is only used when commandline argument --action is delete
    #Deletes file defined by commandline argument --name and adds commit message: Delete --name
    if argument_dict["action"] == "delete":
        try:
            project.files.delete(file_path = argument_dict["name"], commit_message=f"Delete {argument_dict['name']}", branch=argument_dict["branch"])
            print("File removed successfully") 
        except gitlab.exceptions.GitlabDeleteError:
            print("File doesn't exist")

def add_file(argument_dict,project):
    #This function is only used when commandline argument --action is add 
    #Adds new file defined by commandline argument --name gives it content from --file and adds commit message "Add --name"
     if argument_dict["action"] == "add":
        try:
            with open(argument_dict["file"], 'r') as my_file:
                file_content = my_file.read()
        
                   
            project.files.create({'file_path': argument_dict["name"], 'branch': argument_dict["branch"], 'content': file_content, 'commit_message': f"Add {argument_dict['name']}"})
            print("File added successfully")

        except FileNotFoundError:
            print("File cannot be found, check --file argument")

        except gitlab.exceptions.GitlabCreateError:
            print("File already exists")

def update_file(argument_dict,project):
    #This function is only used when commandline argument --action is update
    #Updates existing file defined by commandline argument --name gives it content from --file and adds commit message "Update --name"
     if argument_dict["action"] == "update":
        try:
            with open(argument_dict["file"], 'r') as my_update:
                updated_content = my_update.read()
        
            data = {'branch': argument_dict["branch"],'commit_message': f"Update {argument_dict['name']}" ,'actions': [{'action': 'update','file_path': argument_dict["name"],'content': updated_content}]}
            project.commits.create(data)
            print("File updated successfully")

        except FileNotFoundError:
            print("File cannot be found, check --file argument")




def main():
    argument_dict = Get_Arguments()
    project = connect_to_gitlab(argument_dict)
    issue = create_issue(argument_dict,project)
    delete = delete_file(argument_dict,project)
    add = add_file(argument_dict,project)
    update = update_file(argument_dict,project)


if __name__ == '__main__':
    main()